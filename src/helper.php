<?php
declare(strict_types=1);

use Mpdf\Mpdf;
use Mpdf\MpdfException;
use PhpOffice\PhpWord\Exception\CopyFileException;
use PhpOffice\PhpWord\Exception\CreateTemporaryFileException;
use PhpOffice\PhpWord\IOFactory;
use PhpOffice\PhpWord\Settings;
use PhpOffice\PhpWord\TemplateProcessor;

if (!function_exists('frappe_docx_convert_pdf')) {
    /**
     * doc转pdf
     * @param string $docxFile
     * @param string $pdfFile
     * @param array $replaceFields
     * @throws MpdfException
     * @throws Throwable
     * @throws CopyFileException
     * @throws CreateTemporaryFileException
     * @throws \PhpOffice\PhpWord\Exception\Exception
     * @author yinxu
     * @date 2024/4/3 18:47:33
     */
    function frappe_docx_convert_pdf(string $docxFile, string $pdfFile, array $replaceFields = [])
    {
        if (!is_file($docxFile)) throw new InvalidArgumentException('Docx文件不存在');
        # 设置缓存目录
        Settings::setTempDir(config('office.cache', ''));
        $tempFile = microtime(true) . 'docx';
        try {
            # 替换PDF字段
            if (!empty($replaceFields)) {
                $templateProcessor = new TemplateProcessor($docxFile);
                $templateProcessor->setValues($replaceFields);
                # 保存修改后的docx文件
                $templateProcessor->saveAs($tempFile);
                # 读取缓存文件
                $phpWord = IOFactory::load($tempFile);
            } else {
                $phpWord = IOFactory::load($docxFile);
            }
            $writer = IOFactory::createWriter($phpWord, 'HTML');
            $content = $writer->getWriterPart('Body')->write();
            frappe_html_convert_pdf($content, $pdfFile);
        } finally {
            # 清除缓存文件
            if (is_file($tempFile)) unlink($tempFile);
        }
    }
}

if (!function_exists('frappe_html_convert_pdf')) {
    /**
     * 将Html文件转成pdf文件
     * @param string $html
     * @param string $pdfFile
     * @throws MpdfException
     * @author yinxu
     * @date 2024/4/3 18:38:49
     */
    function frappe_html_convert_pdf(string $html, string $pdfFile)
    {
        $mpdf = new Mpdf([
            'autoScriptToLang' => true,
            'autoLangToFont' => true,
            'useSubstitutions' => true,
        ]);
        $mpdf->WriteHTML($html);
        $mpdf->OutputFile($pdfFile);
    }
}
