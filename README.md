
# frappe-office

php 工具类

## 安装
> composer require yinxu46/frappe-office


## 使用

- docx生成pdf，可替换参数

> frappe_docx_convert_pdf(string $docxFile, string $pdfFile, array $replaceFields = [])

- html生成pdf

> frappe_html_convert_pdf(string $html, string $pdfFile)